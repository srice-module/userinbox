<?php

namespace Modules\UserInbox\Console;

use Illuminate\Console\Command;
use Nwidart\Modules\Facades\Module;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class InstallUserInbox extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'install:userinbox';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installation du module "UserInbox"';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(setting('module.user.inbox.active') == 0) {
            $module = Module::find('UserInbox');
            $module->enable();
            $this->call("migrate");
            setting(["module.user.inbox.active" => 1])->save();
            $this->info("Le module 'User Inbox' est activé");
        }else{
            $this->error("Le module est déja installer");
        }
    }
}
